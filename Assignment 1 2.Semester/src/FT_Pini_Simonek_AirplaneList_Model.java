import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

/* **********************************************************************
 * FT_Pini_Simonek_Airplane.java
 * 
 * Author: Kevin Pini, Andrea Simonek		Date: 15.04.2021
 * 
 * Main model: list of airplanes (FT_Pini_Simonek_Airplane.java) 
 * --> holding a list of airplane objects.
 ********************************************************************** */

public class FT_Pini_Simonek_AirplaneList_Model {
	// Instance variables 
	ArrayList<FT_Pini_Simonek_Airplane> airplaneList ;
	private final String delimiter = "@";
	private boolean dirty = false;

	// -----------------------------------
	// Constructor - populates model from file
	public FT_Pini_Simonek_AirplaneList_Model() {
		this.airplaneList = new ArrayList<FT_Pini_Simonek_Airplane>();
		this.readFromFile();
	}
	
	// -----------------------------------
	// Get dirty
	protected boolean isDirty() {
		return dirty;
	}
	
	// -----------------------------------
	// Add new blank airplane
	public FT_Pini_Simonek_Airplane add() {
		FT_Pini_Simonek_Airplane newAirplane = new FT_Pini_Simonek_Airplane();
		airplaneList.add(newAirplane);	
		return newAirplane;
	}
	
	// -----------------------------------
	// Delete airplane - matches entry by id 
	public boolean delete(String id) {
		for (int i = 0; i < this.airplaneList.size(); i++) {
			if (id == airplaneList.get(i).getId()) {
				airplaneList.remove(i);
				this.dirty = true;
				return true;
			}
		}  
		return false;
	}
	
	// -----------------------------------
	// Updates the list with the provided airplane - matches entry by id 
	public boolean updateAirplane(FT_Pini_Simonek_Airplane value) {
		for (int i = 0; i < airplaneList.size(); i++) { 		      
			String a = value.getId();
			String b = airplaneList.get(i).getId();
			Boolean e = a.equals(b);
			if (value.getId().equals(airplaneList.get(i).getId())) {
				airplaneList.set(i, value);	
				this.dirty = true;
				return true;
			}
	    }
		return false;
	}

	// -----------------------------------
	// Get airplane - matches entry by id 
	public FT_Pini_Simonek_Airplane get(String id) {
		for (int i = 0; i < airplaneList.size(); i++) { 		      
			if (id.equals(airplaneList.get(i).getId())) {
				return airplaneList.get(i);	
			}
	    }
		return null;
	}
	
	// -----------------------------------	
	// SaveToFile method
	public void saveToFile() {
		FileOutputStream fileOut;
		   try {
			   fileOut = new FileOutputStream("AirplaneList.txt");  
			   PrintStream printOut=new PrintStream(fileOut);  
			   printOut.println("Setting file for AirplaneList Project FT_Pini_Simonek_AirplaneList_Model");  
			   for (int i = 0; i < this.airplaneList.size(); i++) {
				   String str = "";
				   FT_Pini_Simonek_Airplane airplane = airplaneList.get(i);
				   // main properties
				   str += airplane.getId() + delimiter;					// 0
				   str += airplane.getManufacturer() + delimiter; 		// 1
				   str += airplane.getModel() + delimiter;				// 2
				   str += airplane.getPropulsion() + delimiter;			// 3
				   str += airplane.getReleaseYear() + delimiter;		// 4
				   str += airplane.getPassengerCapacity() + delimiter;	// 5
				   str += airplane.getWeight() + delimiter;				// 6
				   str += airplane.getMaxAltitude() + delimiter;		// 7
				   str += airplane.getImageFilename() + delimiter;		// 8
				   str += airplane.getPressurizedCabin();				// 9
				   // history list
				   String[] arr = airplane.getHistoryList();
				   for (int j = 0; j < arr.length; j++) {					 
					   str += delimiter + arr[j];						// 10+
				   }
				   printOut.println(str); 
			   }
			   printOut.close();  
			   fileOut.close(); 
			   this.dirty = false;
		   } catch(Exception e) {
			   System.out.println("Error occurred saving the model to 'AirplaneList.txt'");
		   }
	} 
	
	// -----------------------------------
	// Read file 'AirplaneList.txt' and populate model
	public void readFromFile() {
		String str;
		FT_Pini_Simonek_Airplane airplane;
		try {
	        File myObj = new File("AirplaneList.txt");
	        Scanner myScan = new Scanner(myObj);
	        myScan.nextLine();
	        while (myScan.hasNextLine()) {
	        	str = myScan.nextLine();
				airplane = new FT_Pini_Simonek_Airplane();
				String[] arr = str.split(delimiter);
				// main properties
				airplane.setId(arr[0]);
				airplane.setManufacturer(arr[1]);
				airplane.setModel(arr[2]);
				airplane.setPropulsion(arr[3]);
				airplane.setReleaseYear(Integer.parseInt(arr[4]));
				airplane.setPassengerCapacity(Integer.parseInt(arr[5]));
				airplane.setWeight(Double.parseDouble(arr[6]));
				airplane.setMaxAltitude(Integer.parseInt(arr[7]));
				airplane.setImageFilename(arr[8]);
				airplane.setPressurizedCabin(arr[9].equals("true"));
				// history list
				String[] historyList = new String[arr.length - 10];
				for (int i = 10; i < arr.length; i++) {
					historyList[i - 10] = arr[i];
				}
				airplane.setHistoryList(historyList);
				airplaneList.add(airplane);	        
	        }
	        myScan.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred loading the 'AirplaneList.txt' file.");
		}
	}
		
}
