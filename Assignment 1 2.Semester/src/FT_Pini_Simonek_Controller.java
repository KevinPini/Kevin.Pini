import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.image.Image;
import java.util.ArrayList;
import java.util.Optional;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.application.Platform;
import javafx.stage.WindowEvent;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/* **********************************************************************
 * FT_Pini_Simonek_Controller.java
 * Author: Kevin Pini, Andrea Simonek		Date: 15.04.2021
 * Controller --> Eventlistener/handler for the Listview and for each 
 * inputfield in the detail view.
 ********************************************************************** */

public class FT_Pini_Simonek_Controller {
	protected FT_Pini_Simonek_AirplaneList_Model model;
	protected FT_Pini_Simonek_GUI_View view;
	private FT_Pini_Simonek_Airplane activeAirplane = new FT_Pini_Simonek_Airplane();
	
	private boolean busy = false;
	private boolean myGuiError = false;
	private boolean manufacturerError = false;
	private boolean modelError = false;		
	private boolean propulsionError = false;
	private boolean releaseYearError = false;
	private boolean passengerCapacityError = false;
	private boolean weightError = false;
	private boolean maxAltitudeError = false;
	private String errorStyle = "-fx-background-color: #ffc9c9;";
	private String validStyle = "-fx-background-color: #c9e1ff;";
	private String whiteStyle = "-fx-background-color: #ffffff;";

	
	// -----------------------------------
	// Constructor
	public FT_Pini_Simonek_Controller(FT_Pini_Simonek_AirplaneList_Model model, FT_Pini_Simonek_GUI_View view) {
		this.model = model;
		this.view = view;
		this.setupListeners();

		this.updateListView(); // Updates the left list from model data
		
		this.view.listView.getSelectionModel().select(0); // selects the first item which triggers the selection listener and populates the right side      
	}
	
	// -----------------------------------
	// Setup Listeners on ListViews, all Textfields, and buttons
	private void setupListeners() {
		// Stage Close Handler -> dirty?
		Platform.setImplicitExit(false);
		this.view.stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				if (model.isDirty()) {
					Alert alert = new Alert(AlertType.CONFIRMATION);
					alert.setTitle("Unsaved Changes");
					alert.setHeaderText("Confirm");
					alert.setContentText("Close without saving?");
					
					ButtonType saveAndClose = new ButtonType("Save and Close");
					ButtonType close = new ButtonType("Close");
					ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
					
					alert.getButtonTypes().setAll(saveAndClose, close, cancel);
	
					Optional<ButtonType> result = alert.showAndWait();
					if (result.get() == saveAndClose){
						model.saveToFile();
					} else if (result.get() == close){
						
					} else {
						event.consume();
					}
			    }
			}
		});
		
        // List of airplanes 
		this.view.listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<FT_Pini_Simonek_Airplane>() {
			public void changed(ObservableValue<? extends FT_Pini_Simonek_Airplane> observable, FT_Pini_Simonek_Airplane oldValue, FT_Pini_Simonek_Airplane newValue) {
		        // Check whether activeAirplane is not dirty proceed, otherwise ask whether to proceed or cancel
				if (!busy) {
					if (activeAirplane.getDirty()) {
						Alert alert = new Alert(AlertType.CONFIRMATION);
						alert.setTitle("Unsaved Changes");
						alert.setHeaderText("Confirm");
						alert.setContentText("Discard changes without saving?");


						Optional<ButtonType> result = alert.showAndWait();
						if (result.get() == ButtonType.OK){
							updateActiveAirplaneAndGUI(newValue);  
						} else {
							busy = true;
							view.listView.getSelectionModel().select(oldValue);
							busy = false;
						}
						
					} else {
						updateActiveAirplaneAndGUI(newValue);
					}
				}
			}
		});
				
		// Manufacturer
		this.view.tfManufacturer.textProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!busy) {
					Boolean success = activeAirplane.setManufacturer(newValue);
					if (success) {	
						view.tfManufacturer.setStyle(validStyle);
					} else {
						view.tfManufacturer.setStyle(errorStyle);
					}
					manufacturerError = !success;
					view.lbErrorManufacturer.setVisible(!success);
					checkGuiError();
				}
			}
		});
		
		// Model
		this.view.tfModel.textProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!busy) {
					Boolean success = activeAirplane.setModel(newValue);
					if (success) {
						view.tfModel.setStyle(validStyle);
					} else {
						view.tfModel.setStyle(errorStyle);
					}
					modelError = !success;
					view.lbErrorModel.setVisible(!success);
					checkGuiError();
				}
			}
		});
		
		// Propulsion
		this.view.tfPropulsion.textProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!busy) {
					Boolean success = activeAirplane.setPropulsion(newValue);
					if (success) {
						view.tfPropulsion.setStyle(validStyle);
						} else {
						view.tfPropulsion.setStyle(errorStyle);
					}
					propulsionError = !success;
					view.lbErrorPropulsion.setVisible(!success);
					checkGuiError();
				}
			}
		});
		
		// Release Year
		this.view.tfReleaseYear.textProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!busy) {
					Boolean success = true;
					try {
						int val = Integer.parseInt(newValue);
						success = activeAirplane.setReleaseYear(val);
					  } catch (NumberFormatException e) {
					    success = false;
					  }
					if (success) {
						view.tfReleaseYear.setStyle(validStyle);
					} else {
						view.tfReleaseYear.setStyle(errorStyle);
					}
					releaseYearError = !success;
					view.lbErrorReleaseYear.setVisible(!success);
					checkGuiError();
				}
			}
		});
		
		// Passenger Capacity
		this.view.tfPassengerCapacity.textProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!busy) {
					Boolean success = true;
					try {
						int val = Integer.parseInt(newValue);
						success = activeAirplane.setPassengerCapacity(val);
					  } catch (NumberFormatException e) {
					    success = false;
					  }
					if (success) {
						view.tfPassengerCapacity.setStyle(validStyle);
					} else {
						view.tfPassengerCapacity.setStyle(errorStyle);
					}
					passengerCapacityError = !success;
					view.lbErrorPassengerCapacity.setVisible(!success);
					checkGuiError();
				}
			}
		});
		
		// Weight
		this.view.tfWeight.textProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!busy) {
					Boolean success = true;
					try {
						double val = Double.parseDouble(newValue);
						success = activeAirplane.setWeight(val);
					  } catch (NumberFormatException e) {
					    success = false;
					  }
					if (success) {
						view.tfWeight.setStyle(validStyle);
					} else {
						view.tfWeight.setStyle(errorStyle);
					}
					weightError = !success;
					view.lbErrorWeight.setVisible(!success);
					checkGuiError();
				}
			}
		});
		
		// Max. Altitude
		this.view.tfMaxAltitude.textProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!busy) {
					Boolean success = true;
					try {
						int val = Integer.parseInt(newValue);
						success = activeAirplane.setMaxAltitude(val);
					  } catch (NumberFormatException e) {
					    success = false;
					  }
					if (success) {
						view.tfMaxAltitude.setStyle(validStyle);
					} else {
						view.tfMaxAltitude.setStyle(errorStyle);
					}
					maxAltitudeError = !success;
					view.lbErrorMaxAltitude.setVisible(!success);
					checkGuiError();
				}
			}
		});
		
		// Pressurized cabin
		this.view.cbPressurizedCabin.selectedProperty().addListener(new ChangeListener<Boolean>() {
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if (!busy) {
					Boolean success = activeAirplane.setPressurizedCabin(newValue);
					checkGuiError();
				}
			}
		});

		// ImageFile
		this.view.tfImageFilename.textProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		        // Update image from file name if found, otherwise use generic image
				Boolean success = false;
				try {
		        	Image img = new Image(newValue);
		        	view.imgView.setImage(img);
		        	success = true;
		        } 
		        catch (Exception e) {
		        	Image img = new Image("generic_plane_blue.png");
		        	view.imgView.setImage(img);
		        	success = false;
		        }
				if (!busy) {
					if  (success) {
						success = activeAirplane.setImageFilename(newValue);
					}
					view.lbErrorImageFilename.setVisible(!success);
				}
			}
		});
		
		// OK Button
		this.view.buttonOk.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent event) {
		    	// Find the selected index of the list, 
		    	//create a copy of active airplane and set the corresponding listItem.
		    	busy = true;
		    	int index = view.listView.getSelectionModel().getSelectedIndex();
		    	ArrayList<String> listArr = new ArrayList<String>();
		    	for(int i = 0; i < view.historyListView.getItems().size(); i++) {
		    		//Only add item if value not "new entry" or ""
		    		String str = view.historyListView.getItems().get(i);
		    		if (!str.equals("") && !str.equals("new entry")) {
		    			listArr.add(str);
		    		}
		    	}
		    	// Convert listArr to regular array and set activeAirplane's historyList
		    	activeAirplane.setHistoryList(listArr.toArray(new String[0]));	    	
		    	activeAirplane.clearDirty();
		    	FT_Pini_Simonek_Airplane updateModel = new FT_Pini_Simonek_Airplane(activeAirplane);
		    	model.updateAirplane(updateModel);
		    	updateListView();
		    	view.listView.getSelectionModel().select(index);
		    	resetGUI();
		    	busy = false;
		    }
		});
		
		// Cancel Button
		this.view.buttonCancel.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent event) {
		    	// Find the selected index of the list, 
		    	// Reload the active airplane from the selected index.		    	
		    	busy = true;
		    	FT_Pini_Simonek_Airplane reloadModel = view.listView.getSelectionModel().getSelectedItem();
				updateActiveAirplaneAndGUI(reloadModel);
		    	busy = false;
		    }
		});
		
		// Add Button
		this.view.buttonAdd.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent event) {
		    	// Create a new airplane and add to list, then update gui
		    	addAirplane();
		    }
		});
		
		// Save Button
		this.view.buttonSave.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent event) {
		    	// Update AirplaneList.txt File
		    	model.saveToFile();
		    }
		});
		
		// Delete Button
		this.view.buttonDelete.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent event) {
		    	// Delete selected airplane from list
		    	busy = true;
		    	boolean ok = model.delete(activeAirplane.getId());
		    	if (ok) {
			    	updateListView();
		    	}
		    	busy = false;
		    	if (view.listView.getItems().size() == 0) {
		    		addAirplane();
		    	}
		    	view.listView.getSelectionModel().select(0);
		    }
		});	
	
		// HistoryList Event Listener		
		this.view.historyListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
		        // Enable buttons on selection change, could not find a content change listener
				if (!busy) {
					checkGuiError();
				}
			}
		});
		
		// Add history List Button
		this.view.buttonListAdd.setOnAction(new EventHandler<ActionEvent>() {
		    public void handle(ActionEvent event) {
		    	view.historyListView.getItems().add("new entry");
		    }
		});
	}
	
	// -----------------------------------
	// Add new Airplane to mode, update list view and select it + reset GUI fields
	private void addAirplane() {
    	busy = true;
    	FT_Pini_Simonek_Airplane newAirplane = model.add();
    	updateListView();
		updateActiveAirplaneAndGUI(newAirplane);
		view.listView.getSelectionModel().select(view.listView.getItems().size() - 1);  
    	busy = false;
		view.tfManufacturer.setText("");
		view.tfModel.setText("");
		view.tfPropulsion.setText("");
		view.tfReleaseYear.setText("");
		view.tfPassengerCapacity.setText("");
		view.tfWeight.setText("");
		view.tfMaxAltitude.setText("");
		view.tfImageFilename.setText("");
		view.buttonCancel.setDisable(true);
	}

	// -----------------------------------
	// Check Error of all inputs
	private void checkGuiError() {
		boolean[] guiError = {manufacturerError, modelError, propulsionError, releaseYearError, passengerCapacityError, weightError, maxAltitudeError};
		view.buttonOk.setDisable(false);
		view.buttonCancel.setDisable(false);
		for(boolean value: guiError){
			  if(value){ 
				  view.buttonOk.setDisable(true);
				  break;
				  }
			}
	}

	// -----------------------------------
	// Create a copy of the selected airplane and set this as the activeAirPlane + update GUI
	private void updateActiveAirplaneAndGUI(FT_Pini_Simonek_Airplane p) {
		this.busy = true;
		this.activeAirplane = new FT_Pini_Simonek_Airplane(p);
		this.view.tfManufacturer.setText(this.activeAirplane.getManufacturer());
		this.view.tfModel.setText(this.activeAirplane.getModel());
		this.view.tfPropulsion.setText(this.activeAirplane.getPropulsion());
		this.view.tfReleaseYear.setText(Integer.toString(this.activeAirplane.getReleaseYear()));
		this.view.tfPassengerCapacity.setText(Integer.toString(this.activeAirplane.getPassengerCapacity()));
		this.view.tfWeight.setText(Double.toString(this.activeAirplane.getWeight()));
		this.view.tfMaxAltitude.setText(Integer.toString(this.activeAirplane.getMaxAltitude()));
		this.view.tfImageFilename.setText(this.activeAirplane.getImageFilename());
		this.view.cbPressurizedCabin.setSelected(this.activeAirplane.getPressurizedCabin());
    	this.view.observableListHistory = FXCollections.observableArrayList(this.activeAirplane.getHistoryList());
        this.view.historyListView.setItems(this.view.observableListHistory);

		resetGUI();
		this.busy = false;
	}
	
	// -----------------------------------
	// Update listView from model (left)
    private void updateListView() {
    	this.view.observableList = FXCollections.observableArrayList(this.model.airplaneList);
        this.view.listView.setItems(this.view.observableList); 
    }
    
	// -----------------------------------
    // Reset GUI (clear errors, reset inputs, reset buttons, background color of inputs)
    private void resetGUI() {
		this.view.tfManufacturer.setStyle(whiteStyle);
		this.view.tfModel.setStyle(whiteStyle);
		this.view.tfPropulsion.setStyle(whiteStyle);
		this.view.tfReleaseYear.setStyle(whiteStyle);
		this.view.tfPassengerCapacity.setStyle(whiteStyle);
		this.view.tfWeight.setStyle(whiteStyle);
		this.view.tfMaxAltitude.setStyle(whiteStyle);
		this.view.tfImageFilename.setStyle(whiteStyle);
		this.view.cbPressurizedCabin.setStyle(whiteStyle);
		manufacturerError = false;
		modelError = false;
		propulsionError = false;
		releaseYearError = false;
		passengerCapacityError = false;
		weightError = false;
		maxAltitudeError = false;
		view.buttonOk.setDisable(true);
		view.buttonCancel.setDisable(true);
		view.lbErrorManufacturer.setVisible(false);
		view.lbErrorModel.setVisible(false);
		view.lbErrorPropulsion.setVisible(false);
		view.lbErrorReleaseYear.setVisible(false);
		view.lbErrorPassengerCapacity.setVisible(false);
		view.lbErrorWeight.setVisible(false);
		view.lbErrorMaxAltitude.setVisible(false);
		view.lbErrorImageFilename.setVisible(false);
    }
}