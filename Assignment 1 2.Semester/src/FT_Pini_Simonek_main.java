import javafx.application.Application;
import javafx.stage.Stage;

/* **********************************************************************
 * FT_Pini_Simonek_main.java
 * Authors: Kevin Pini, Andrea Simonek		Date: 30.03.2021
 * Main class instantiates model, view, controller and starts application 
 ********************************************************************** */

public class FT_Pini_Simonek_main extends Application{
	private FT_Pini_Simonek_GUI_View view;
	private FT_Pini_Simonek_AirplaneList_Model model;
	private FT_Pini_Simonek_Controller controller;
	
	public static void main(String[] args) {
				launch();
	}
	
	// Instantiate model, view, controller and start application
	public void start(Stage stage) throws Exception {
		this.model = new FT_Pini_Simonek_AirplaneList_Model();
		this.view = new FT_Pini_Simonek_GUI_View(stage, this.model);
		this.controller = new FT_Pini_Simonek_Controller(this.model, this.view);
		this.view.start();
	}
}
