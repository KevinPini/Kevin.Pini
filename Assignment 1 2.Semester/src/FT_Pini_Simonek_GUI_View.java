import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.control.Separator;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.VPos;

/* **********************************************************************
 * FT_Pini_Simonek_GUI.java
 * Authors: Kevin Pini, Andrea Simonek		Date: 15.04.2021
 * Viewer class displays a GUI and shows information of different airplanes. 
 * User can edit or delete the data
 ********************************************************************** */

public class FT_Pini_Simonek_GUI_View {
   

//create private variables
	protected Stage stage;
	protected FT_Pini_Simonek_AirplaneList_Model model;
	
	protected ListView<FT_Pini_Simonek_Airplane> listView = new ListView<FT_Pini_Simonek_Airplane>();
	protected ObservableList<FT_Pini_Simonek_Airplane> observableList;
	protected ObservableList<String> observableListHistory;

	private Image[] airplaneImages;
	protected ImageView imgView;
	protected Button buttonAdd, buttonSave, buttonOk, buttonCancel, buttonDelete, buttonListAdd;
	protected TextField tfManufacturer, tfModel, tfPropulsion, tfReleaseYear, tfPassengerCapacity, tfWeight, tfMaxAltitude, tfImageFilename;
	protected Label lbErrorManufacturer, lbErrorModel, lbErrorPropulsion, lbErrorReleaseYear, lbErrorPassengerCapacity, lbErrorWeight, lbErrorMaxAltitude, lbErrorImageFilename;
    protected CheckBox cbPressurizedCabin;
	protected ListView<String> historyListView;

	
// this class displays a split pane with a list of airplanes on the left
// and an image of the selected item on the right as well as some information
  	
	public FT_Pini_Simonek_GUI_View(Stage stage, FT_Pini_Simonek_AirplaneList_Model model)
    {
		this.stage = stage;
		this.model = model;
        
        // Picture
		this.imgView = new ImageView();
        this.imgView.setPreserveRatio(true);
        this.imgView.setFitHeight(200);
        
        // History view
        this.historyListView = new ListView<String>();
        this.historyListView.setMaxHeight(208);
        this.historyListView.setEditable(true);
        this.historyListView.setCellFactory(TextFieldListCell.forListView());
        
        StackPane imgPane = new StackPane(this.imgView);
        imgPane.setMinWidth(200);
                    
        this.imgView.fitWidthProperty().bind(imgPane.widthProperty());
        //// this.imgView.fitHeightProperty().bind(imgPane.heightProperty());  // Does not work
        
        // Buttons  (inside hBoxButtons)
        this.buttonAdd = new Button("Add");
        this.buttonSave = new Button("Save");
        this.buttonOk = new Button("Ok");
        this.buttonCancel = new Button("Cancel");
        this.buttonDelete = new Button("Delete");
        this.buttonListAdd = new Button("+");
        this.buttonListAdd.setMaxSize(30, 20);
        buttonListAdd.setStyle("-fx-background-radius: 15; -fx-background-color: #1c67ad; -fx-text-fill: white; -fx-padding: 0;");
        buttonAdd.setStyle("-fx-background-radius: 15; ");
        buttonSave.setStyle("-fx-background-radius: 15; ");
        buttonOk.setStyle("-fx-background-radius: 15; -fx-background-color: #1c67ad; -fx-text-fill: white;");
        buttonOk.setPrefSize(50, 20);
        buttonCancel.setStyle("-fx-background-radius: 15; ");
        buttonDelete.setStyle("-fx-background-radius: 15; ");
        
        HBox hBoxButtons1 = new HBox(this.buttonAdd, this.buttonSave);
        hBoxButtons1.setSpacing(10);
        hBoxButtons1.setPadding(new Insets(5, 5, 5, 5));
        
        HBox hBoxButtons2 = new HBox(this.buttonOk, this.buttonCancel, this.buttonDelete);
        hBoxButtons2.setSpacing(10);
        hBoxButtons2.setPadding(new Insets(5, 5, 5, 5));
        
        Region autoGrowRegion1 = new Region();
        HBox.setHgrow(autoGrowRegion1, Priority.ALWAYS);

        HBox hBoxButtons = new HBox(hBoxButtons1, autoGrowRegion1, hBoxButtons2);
       
        
        // 1: V-Box (Top: (2) Bottom: Buttons)
        BorderPane borderPane1 = new BorderPane();
        
        // 3: Split-Pane  (Top: picture, Bottom: (4))
        SplitPane pane3 = new SplitPane();
        pane3.setOrientation(Orientation.VERTICAL);
        pane3.setDividerPositions(0.25);
        
        // 4: Grid-Pane	  (Left: properties, Right: listOfOrders)
        GridPane gridPane4 = new GridPane();
	    gridPane4.setHgap(10);
	    gridPane4.setVgap(5);
	    gridPane4.setPadding(new Insets(10, 10, 10, 10));

	    this.tfManufacturer = new TextField();
	    this.tfManufacturer.setPromptText("e.g. Boeing");
	    
	    this.tfModel = new TextField();
	    this.tfModel.setPromptText("e.g. 747");
	    
	    this.tfPropulsion = new TextField();
	    this.tfPropulsion.setPromptText("e.g. 4 jet engines");
	    
	    this.tfReleaseYear = new TextField();
	    this.tfReleaseYear.setPromptText("e.g. 2012");
	    
	    this.tfPassengerCapacity = new TextField();
	    this.tfPassengerCapacity.setPromptText("e.g. 540");
	    
	    this.tfWeight = new TextField();
	    this.tfWeight.setPromptText("e.g. 510");
	    
	    this.tfMaxAltitude = new TextField();
	    this.tfMaxAltitude.setPromptText("e.g. 13000");
	    
	    this.cbPressurizedCabin = new CheckBox();
	    
	    this.tfImageFilename = new TextField();
	    this.tfImageFilename.setPromptText("Optional");
	    
	    this.lbErrorManufacturer = new Label("Required field");
	    this.lbErrorModel = new Label("Required field");
	    this.lbErrorPropulsion = new Label("Required field");
	    this.lbErrorReleaseYear = new Label("1900 - now");
	    this.lbErrorPassengerCapacity = new Label("1-1000");
	    this.lbErrorWeight = new Label("1-1000");
	    this.lbErrorMaxAltitude = new Label("0 - 30000");
	    this.lbErrorImageFilename = new Label("File not found");
	    
	    this.lbErrorManufacturer.setTextFill(Color.RED);
	    this.lbErrorModel.setTextFill(Color.RED);
	    this.lbErrorPropulsion.setTextFill(Color.RED);
	    this.lbErrorReleaseYear.setTextFill(Color.RED);
	    this.lbErrorPassengerCapacity.setTextFill(Color.RED);
	    this.lbErrorWeight.setTextFill(Color.RED);
	    this.lbErrorMaxAltitude.setTextFill(Color.RED);
	    this.lbErrorImageFilename.setTextFill(Color.RED);
     
        borderPane1.setLeft(this.listView);
        borderPane1.setBottom(hBoxButtons);
        borderPane1.setCenter(pane3);
        pane3.getItems().addAll(imgPane, gridPane4);
        gridPane4.add(new Label("Manufacturer"), 0, 0);
        gridPane4.add(new Label("Model"), 0, 1);
        gridPane4.add(new Label("Propulsion"), 0, 2);
        gridPane4.add(new Label("Release year"), 0, 3);
        gridPane4.add(new Label("Passenger capacity"), 0, 4);
        gridPane4.add(new Label("Weight [tons]"), 0, 5);
        gridPane4.add(new Label("Max. altitude [m]"), 0, 6);
        gridPane4.add(new Label("Picture filename"), 0, 7);
        gridPane4.add(new Label("Pressurized cabin?"), 0, 8);
        gridPane4.add(new Label("History of generations"), 3, 0);
        gridPane4.add(this.historyListView, 3, 1);
        GridPane.setRowSpan(historyListView, 8);
        GridPane.setColumnSpan(historyListView, 2);
        GridPane.setValignment(historyListView, VPos.TOP);
        gridPane4.add(this.buttonListAdd, 4, 0);
        GridPane.setHalignment(buttonListAdd, HPos.RIGHT);
        gridPane4.add(this.tfManufacturer, 1, 0);
        gridPane4.add(this.tfModel, 1, 1);
        gridPane4.add(this.tfPropulsion, 1, 2);
        gridPane4.add(this.tfReleaseYear, 1, 3);
        gridPane4.add(this.tfPassengerCapacity, 1, 4);
        gridPane4.add(this.tfWeight, 1, 5);
        gridPane4.add(this.tfMaxAltitude, 1, 6);
        gridPane4.add(this.tfImageFilename, 1, 7);
        gridPane4.add(this.cbPressurizedCabin, 1, 8);  
        gridPane4.add(this.lbErrorManufacturer, 2, 0);
        gridPane4.add(this.lbErrorModel, 2, 1);
        gridPane4.add(this.lbErrorPropulsion, 2, 2);
        gridPane4.add(this.lbErrorReleaseYear, 2, 3);
        gridPane4.add(this.lbErrorPassengerCapacity, 2, 4);
        gridPane4.add(this.lbErrorWeight, 2, 5);
        gridPane4.add(this.lbErrorMaxAltitude, 2, 6);
        gridPane4.add(this.lbErrorImageFilename, 2, 7);
        
        VBox leftControl = new VBox(new Label("left VBox"));
        Separator separatorV = new Separator(Orientation.VERTICAL);
        

       // Initialize Stage
        Scene scene = new Scene(borderPane1, 1000, 650, Color.LIGHTBLUE);
        stage.setTitle("Airplane information system");
        stage.setScene(scene);
       
    }
            
    
    //  Start GUI
    //--------------------------------------------------------------------

	public void start() {
		stage.show();
		
	}

}




