import java.time.YearMonth;
import java.util.Arrays;
import java.util.UUID;

/* **********************************************************************
 * FT_Pini_Simonek_Airplane.java
 * Author: Kevin Pini, Andrea Simonek		Date: 15.04.2021
 * Airplane Class for a single aviation object.
 * --> Methods: getters and setters, cleartDirty 
 * (Setters perform validation and return true if ok, false if invalid)
 ********************************************************************** */

public class FT_Pini_Simonek_Airplane {
	private String id;
	private boolean dirty;
	private String manufacturer;
	private String model;
	private String propulsion;
	private int releaseYear;
	private int passengerCapacity;
	private double weight;
	private int maxAltitude;
	private String imageFilename;
	private boolean pressurizedCabin;
	private String[] historyList = {};
	
	static int year = YearMonth.now().getYear();
		
	// -----------------------------------
	// Constructor () -> blank
	public FT_Pini_Simonek_Airplane() {
		this.id = UUID.randomUUID().toString();
	}
	
	// -----------------------------------
	// Constructor (airplane) -> clone airplane
	public FT_Pini_Simonek_Airplane(FT_Pini_Simonek_Airplane p) {
		this.dirty = false;
		this.id = p.id;
		this.manufacturer = p.manufacturer;
		this.model = p.model;
		this.propulsion = p.propulsion;
		this.releaseYear = p.releaseYear;
		this.passengerCapacity = p.passengerCapacity;
		this.weight = p.weight;
		this.maxAltitude = p.maxAltitude;
		this.imageFilename = p.imageFilename;
		this.pressurizedCabin = p.pressurizedCabin;
		this.historyList = Arrays.copyOf(p.historyList, p.historyList.length);
	}

	
	//-------------------------------------------------------------------------
	// Getters and Setters
	//-------------------------------------------------------------------------
	// manufacturer (String, not empty) 
	public String getManufacturer() {
		return manufacturer;
	}
	public boolean setManufacturer(String manufacturer) {
		if (!manufacturer.equals("")) {
			if (manufacturer != this.manufacturer) {
				this.manufacturer = manufacturer;
				this.dirty = true;
			}
			return true;
		} else {
			return false;
		}
	}
	
	//------------------------------------------------------------------------
	// model (String, not empty) 
	public String getModel() {
		return this.model;
	}
	public boolean setModel(String model) {
		if (!model.equals("")) {
			if (model != this.model) {
				this.model = model;
				this.dirty = true;
			}
			return true;
		} else {
			return false;
		}	
	}
	
	//------------------------------------------------------------------------
	// propulsion (String, not empty) 
	public String getPropulsion() {
		return this.propulsion;
	}
	public boolean setPropulsion(String propulsion) {
		if (!propulsion.equals("")) {
			if (propulsion != this.propulsion) {
				this.propulsion = propulsion;
				this.dirty = true;
			}
			return true;
		} else {
			return false;
		}	
	}
	
	//------------------------------------------------------------------------
	// releaseYear (integer, 1900 - now) 
	public int getReleaseYear() {
		return this.releaseYear;
	}
	public boolean setReleaseYear(int releaseYear) {
		if (releaseYear >= 1900 && releaseYear <= this.year) {
			if (releaseYear != this.year) {
				this.releaseYear = releaseYear;
				this.dirty = true;
			}
			return true;
		} else {
			return false;
		}	
	}
	
	//------------------------------------------------------------------------
	// passengerCapacity (integer, 1-1000) 
	public int getPassengerCapacity() {
		return this.passengerCapacity;
	}
	public boolean setPassengerCapacity(int passengerCapacity) {
		if (passengerCapacity > 0 && passengerCapacity <= 1000) {
			if (passengerCapacity != this.passengerCapacity) {
				this.passengerCapacity = passengerCapacity;
				this.dirty = true;
			}
			return true;
		} else {
			return false;
		}	
	}
	
	//------------------------------------------------------------------------
	// weight (double, 1-1000) 
	public double getWeight() {
		return this.weight;
	}
	public boolean setWeight(double weight) {
		if (weight > 0 && weight <= 1000) {
			if (weight != this.weight) {
				this.weight = weight;
				this.dirty = true;
			}
			return true;
		} else {
			return false;
		}	
	}
	
	//------------------------------------------------------------------------
	// maxAltitude (integer 0 - 30000)
	public int getMaxAltitude() {
		return this.maxAltitude;
	}
	public boolean setMaxAltitude(int maxAltitude) {
		if (maxAltitude > 0 && maxAltitude <= 30000) {
			if (maxAltitude != this.maxAltitude) {
				this.maxAltitude = maxAltitude;
				this.dirty = true;
			}
			return true;
		} else {
			return false;
		}	
	}
	
	//------------------------------------------------------------------------
	// pressurizedCabin (boolean)
	public boolean getPressurizedCabin() {
		return this.pressurizedCabin;
	}
	public boolean setPressurizedCabin(boolean pressurizedCabin) {
		if (pressurizedCabin != this.pressurizedCabin) {
			this.pressurizedCabin = pressurizedCabin;
			this.dirty = true;
		}
		return true;	
	}
	
	//------------------------------------------------------------------------
	// photo (String, filename)
	public String getImageFilename() {
		return this.imageFilename;
	}
	public boolean setImageFilename(String imageFilename) {
		if (imageFilename != this.imageFilename) {
			this.imageFilename = imageFilename;
			this.dirty = true;
		}
		return true;
	}
	
	//------------------------------------------------------------------------
	// historyList (String array)
	public String[] getHistoryList() {
		return this.historyList;
	}
	public boolean setHistoryList(String[] historyList) {
			this.historyList = historyList;
			this.dirty = true;	
			return true;
	}
	
	//------------------------------------------------------------------------
	// id (String - UUID format)
	public String getId() {
		return this.id;
	}
	public boolean setId(String id) {
			this.id = id;
			return true;
	}

	//------------------------------------------------------------------------
	// dirty (boolean, gets set when changing any value)
	public boolean getDirty() {
		return this.dirty;
	}
	public void clearDirty() {
		this.dirty = false;
	}

	//------------------------------------------------------------------------
	// toString() Returns 'manufacturer - model' or 'New airplane'
	public String toString() { 
		String txt = this.manufacturer + " - " + this.model;
		if (this.manufacturer == null) {
			return "New airplane";
		} else {
			return txt;
		}
		
	}

}
