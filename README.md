# Assignment 1

 Readme.md								Assignment 1
  
 Author: Kevin Pini, Andrea Simonek		Date: 15.04.2021


This application displays a list of airplanes on the left side. 
When selecting (clicking on list item or navigating up down) the right panel displays all details.
All properties can be edited in the detail view, but are not automatically applied. 
OK button accepts changes, Cancel button reloads properties from list. 
When attempting to select a new airplane or adding a new one with unsaved changes a confirmation dialog will be presented.

The data is stored in a file 'AirplaneList.txt' when pressing save button.
When starting the application the list is populated from this file data and the first item is selected.


List of airplanes (left):
------------------------
- Select by mouse click or up/down key  ->  Updates the detail view.

Buttons:
-------
- Add  		->  Adds a new blank airplane to the list and updates the detail view.
- Save  	->  Saves the list data to file ('AirplaneList.txt').
- OK    	->  Applies changes in detail view to selected airplane in list.
- Cancel  	->  Discards changes in detail view and reloads values from selected airplane in list (no confirmation).
- Delete  	->  Deletes the selected airplane from the list and selects the first item (no confirmation).


Detail view:
-----------
The detail pane on the right shows all properties of the selected airplane to the left.
All properties can be edited.

- Picture: 		  is automatically loaded when setting the picture filename input.
    -> The file must be manually copied into the source folder.
- Text fields: 	  validation: must not be blank (manufacturer, model, propulsion), Picture filename optional.
- Number fields:  validation: must be in range (release year, passenger capacity, weight, max. altitude).
- Check box: 	  No validation.

All fields have an empty prompt message. When invalid the field is highlighted red and an error message is shown.


History of generations list:
---------------------------
- (+) button 	->  Adds a new entry to the list.
- Double click 	->  Edit item.
- Enter			->  Accepts new value while editing.
--> Caution: when editing list item clicking another item will discard changes (see issues below).


Exit application:
----------------
Before exiting the main model is checked for dirty flag and a confirmation dialog is shown in case of unsaved changes.


Issues:
------
- Picture file must be manually copied into the source folder. 
	-> Not sure how to implement a file picker which copy pastes files, needs more time.
- Picture space can be resized vertically but picture size is fixed. 
	-> auto-size was not working properly, the image was only growing but could not shrink anymore.
	
- When editing the history list ENTER must be pressed to accept, otherwise changes will be lost.
- History list sets dirty flag on selection change, since no listener was found for cell contents change. 
	-> Probably a custom cell editor must be created to implement usual behavior as in Excel, needs more time.





Consists of picture on the top, left side is the plane description and right side is a list of historic information on generations.
- Textfields contain prompt text (disappears when active)
- If invalid input --> disable ok button
- Once edited a field and clicked on another airplane a confirmation dialog pops up (continue without saving or cancel) only if something is valid, save only the valid inputs
- confirmation dialog does not pop up if switching to another plane while editing in the history list --> will not save unless user clicks ok button first
- to add a entry in the history list click the + button 
- to save the entry in the history list press the ENTER key, otherwise wont save
- to save the history list when changing to another plane press Ok button, otherwise list will not be saved
- picture must be copied into the project to be found
- to add a picture type in the picture file name
- To delete the entire airplane entry from the listview on the left side, click Delete
- When closing the program confirmation dialog pops up (save, don't save, cancel)
